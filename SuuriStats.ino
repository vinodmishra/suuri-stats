#define CODE_VERS  "0.1"  // Code version number
/*
  SuuriStats - Version 0.1 
  Vinod Mishra

  ----------------------------------------------------------------------------------
  ATTRIBUTION
  ----------------------------------------------------------------------------------
  Heavily based on 
  PHATSTATS TFT PC Performance Monitor 
  Rupert Hirst & Colin Conway © 2016 - 2021
  http://tallmanlabs.com
  http://runawaybrainz.blogspot.com/

  ----------------------------------------------------------------------------------
  Licence
  --------
  GPL v2

  ----------------------------------------------------------------------------------
  NOTES
  ----------------------------------------------------------------------------------
  - Only ATmega32U4 boards are supported
  - Only supports SH1106 or SSD1306 i2c displays supported
  - Needs the counterpart desktop application Wee Hardware Stat Server available at https://gitlab.com/vinodmishra/wee-hardware-stat-server which uses LibreHardwareMonitor lib to detect the hardware.
  - Does not support integrated GPUs. 

  ----------------------------------------------------------------------------------
  NOTES
  ----------------------------------------------------------------------------------
    Version 0.1   : Refactor and reorganisation to clean up duplicate code and avoid doing extra procesing when getting incoming data.
                    Switched to a new data format which uses WeeHardwareStatmonitor instead which is easier to parse.
                    Removed Neopixel as it frees up some memory and I have no need for it at the moment. 

  ----------------------------------------------------------------------------------
  BOARD MANAGER SEEDUINO XIAO
  ----------------------------------------------------------------------------------
  https://wiki.seeedstudio.com/Seeeduino-XIAO/
  Click on File > Preference, and fill Additional Boards Manager URLs with the url below:
  https://files.seeedstudio.com/arduino/package_seeeduino_boards_index.json
  
  ----------------------------------------------------------------------------------
  LIBRARIES
  ----------------------------------------------------------------------------------
  Adafruit GFX Library
  https://github.com/adafruit/Adafruit-GFX-Library

  Adafruit ILI9341
  https://github.com/adafruit/Adafruit_ILI9341

  Hookup Guide
  https://runawaybrainz.blogspot.com/2021/03/phat-stats-ili9341-tft-display-hook-up.html

  Licence
  --------
  GPL v2

  This Sketch Requires WeeHardwareStatServer
*/


#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Fonts/Org_01.h>

#include "Configuration_Settings.h" // load settings
#include "Bitmaps.h"

//---------------------------------------------------------------------------------------

/*onboard XIAO BUILT in LED for TX*/
#define TX_LEDPin 13

//----------------------------------------------------------------------------

/* ILI9321 TFT setup */
#include <Adafruit_ILI9341.h>  // v1.5.6 Adafruit Standard

/* ATSAMD21 SPi Hardware only for speed*/
#define TFT_CS     5
#define TFT_DC     7
#define TFT_RST    9

/* These pins do not have to be defined as they are hardware pins */
//Connect TFT_SCLK to pin   8
//Connect TFT_MOSI to pin   10

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC, TFT_RST); // Use hardware SPI

//-----------------------------------------------------------------------------

/* Screen TFT backlight Pin */
int TFT_backlight_PIN = 4;

//-----------------------------------------------------------------------------

/* Display screen rotation  0, 1, 2 or 3 = (0, 90, 180 or 270 degrees)*/
int ASPECT = 0; //Do not adjust,

/* More Display stuff*/
int displayDraw = 0;

//-----------------------------------------------------------------------------

/* Timer for active connection to host*/
boolean activeConn = false;
long lastActiveConn = 0;
boolean bootMode = true;

/* Vars for serial input*/
String inputString = "";
boolean stringComplete = false;

//-----------------------------  TFT Colours  ---------------------------------

#define ILI9341_TEST        0x6A4E
#define ILI9341_BLACK       0x0000
#define ILI9341_WHITE       0xFFFF
#define ILI9341_GREY        0x7BEF
#define ILI9341_LIGHT_GREY  0xC618
#define ILI9341_GREEN       0x07E0
#define ILI9341_LIME        0x87E0
#define ILI9341_BLUE        0x001F
#define ILI9341_RED         0xF800
#define ILI9341_AQUA        0x5D1C
#define ILI9341_YELLOW      0xFFE0
#define ILI9341_MAGENTA     0xF81F
#define ILI9341_CYAN        0x07FF
#define ILI9341_DARK_CYAN   0x03EF
#define ILI9341_ORANGE      0xFCA0
#define ILI9341_PINK        0xF97F
#define ILI9341_BROWN       0x8200
#define ILI9341_VIOLET      0x9199
#define ILI9341_SILVER      0xA510
#define ILI9341_GOLD        0xA508
#define ILI9341_NAVY        0x000F
#define ILI9341_MAROON      0x7800
#define ILI9341_PURPLE      0x780F
#define ILI9341_OLIVE       0x7BE0
//--------------------------------

void setup() {

  Serial.begin(9600);  //  USB Serial Baud Rate
  inputString.reserve(200); // String Buffer

  /* Set up PINs*/
  pinMode(TFT_backlight_PIN, OUTPUT); // declare backlight pin to be an output:
  pinMode(TX_LEDPin, OUTPUT); //  Builtin LED /  HIGH(OFF) LOW (ON)

  backlightOFF();

  /* TFT SETUP */

  delay(1000); // Give the micro time to initiate the SPi bus
  tft.begin(); //ILI9341
  tft.setRotation(ASPECT);// Rotate the display :  0, 1, 2 or 3 = (0, 90, 180 or 270 degrees)

  /* stops text wrapping*/
  tft.setTextWrap(false); // Stop  "Loads/Temps" wrapping and corrupting static characters

  /* Clear Screen*/
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);

  splashScreen();
  delay(1000);
}

/* End of Set up */

void loop() {

  serialEvent();          // Check for Serial Activity
  activityChecker();      // Turn off screen when no activity
  digitalWrite(TX_LEDPin, HIGH);    // turn the LED off HIGH(OFF) LOW (ON)
  DisplayStats();
}

/* END of Main Loop */

//-----------------------------  Serial Events -------------------------------
/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {

  while (Serial.available()) {
    //while (Serial.available() > 0) {
    // get the new byte:
    char inChar = (char)Serial.read();
    //Serial.print(inChar); // Debug Incoming Serial

    // add it to the inputString:
    inputString += inChar;
    // if the incoming character has '|' in it, set a flag so the main loop can do something about it:
    if (inChar == '|') {
      stringComplete = true;

      delay(Serial_eventDelay);   //delay screen event to stop screen data corruption
      digitalWrite(TX_LEDPin, LOW);   // turn the LED off HIGH(OFF) LOW (ON)
    }
  }
}

//----------------------------- ActivityChecker  -------------------------------
void activityChecker() {

  if (millis() - lastActiveConn > lastActiveDelay)

    activeConn = false;
  else
    activeConn = true;

  if (!activeConn) {
    /* Clear Screen & Turn Off Backlight, */
    tft.fillScreen(ILI9341_BLACK);
    backlightOFF ();
    displayDraw = 0;
  }

}

//----------------------------- TFT Backlight  -------------------------------

void backlightON () {
  analogWrite(TFT_backlight_PIN, 255); // TFT turn on backlight
}

void backlightOFF () {
  analogWrite(TFT_backlight_PIN, 0);        // TFT turn off backlight,
}

//----------------------------- Splash Screens --------------------------------
void splashScreen() {

  /* Initial Boot Screen, */

  tft.setRotation(1);// Rotate the display at the start:  0, 1, 2 or 3 = (0, 90, 180 or 270 degrees)

  tft.setFont(&Org_01);
  tft.fillScreen(ILI9341_BLACK);

  tft.drawRoundRect  (2, 2  , 316, 236, 8,    ILI9341_RED);

  tft.drawBitmap(84, 20, ST_LOGO,  150, 118, ILI9341_YELLOW);

  tft.setTextSize(3);
  tft.setCursor(50, 155);
  tft.setTextColor(ILI9341_WHITE);
  tft.print("Suuri Stats ");
  tft.print (CODE_VERS);
  
  tft.setTextSize(2);
  tft.setCursor(60, 180);
  tft.setTextColor(ILI9341_SILVER);
  tft.print("PC Hardware Monitor");

  tft.setTextSize(3);
  tft.setCursor(55, 200);
  tft.setTextColor(ILI9341_RED);
  tft.print("vinodmishra.com");

  tft.setTextColor(ILI9341_WHITE);
  tft.setFont(); // Set Default Adafruit GRFX Font
  tft.setTextSize(1);
  tft.setCursor(45, 220);
  tft.print("Use WeeHardwareStatServer 0.4 or above");

  backlightON();
  delay(6000);
}

String getValue(String key) {
    String searchKey = key + ':';
    int keyIndex = inputString.indexOf(searchKey);
    if (keyIndex > -1) {
        int valIndex = inputString.indexOf("#", keyIndex);
        return inputString.substring(keyIndex + searchKey.length(), valIndex);
    }
    return "0";
}

String getCpuName() {
    String cpuName = getValue("CN");
    int cpuNameStart;
    if (cpuName.indexOf("Intel") > -1) {
        cpuNameStart = 10;
    } else {
        cpuNameStart = 4;
    }
    cpuName = cpuName.substring(cpuNameStart);

    if (cpuName.length() > 20) {
        return cpuName.substring(0, cpuName.lastIndexOf(" "));
    }
    else {
        return cpuName;
    }
   
}

String getGpuName() {
    String gpuName = getValue("GN");
    int gpuNameStart;
    if (gpuName.indexOf("NVIDIA") > -1) {
        gpuNameStart = 15;
    } else {
        gpuNameStart = 4;
    }
    return gpuName.substring(gpuNameStart);
}
