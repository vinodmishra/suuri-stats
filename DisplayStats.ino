/*Optimised for ILI9341 320 x 240 in landscape*/

void DisplayStats () {

  /* TFT DRAW STATS */
  if (stringComplete) {

    if (bootMode) {

      //splashScreen2();

      tft.fillScreen(ILI9341_BLACK);
      bootMode = false;
    }

    lastActiveConn = millis();


    //--------------------------------------- Display Background ----------------------------------------------------

    backlightON (); //Turn ON display when there is  activity


    tft.setRotation(1);// Rotate the display at the start:  0, 1, 2 or 3 = (0, 90, 180 or 270 degrees)
    tft.setFont(); // set to default Adafruit library font
    tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);

#ifdef Debug
    tft.setTextColor(ILI9341_WHITE, ILI9341_RED); // used to stop flickering when updating digits that do not increase in length. CPU/GPU load still need a clear box on the end digits
#endif


    /* Display Background */
    //----------------------------------------- Boost/Turbo Clear Box----------------------------------------------


    /* CPU Portrait TURBO/TJMAX, */
    tft.fillRoundRect  (107, 91, 86, 20, 4, ILI9341_BLACK);   //
    tft.drawRoundRect  (106, 90, 88, 22, 4, ILI9341_SILVER); //

    /* GPU Portrait BOOST/TJMAX, */
    tft.fillRoundRect  (107, 208, 86, 20, 4, ILI9341_BLACK);   //
    tft.drawRoundRect  (106, 207, 88, 22, 4, ILI9341_SILVER); //

    /* CPU  Freq Line */
    tft.drawFastHLine(110, 47, 85,  ILI9341_SILVER);

    /*GPU Memory Used Line */
    tft.drawFastHLine(110, 167, 85, ILI9341_SILVER);

    
    tft.drawFastHLine(210, 91, 101,  ILI9341_SILVER);

    //--------------------------------------Borders----------------------------------------

    /* (X  ,Y ,  W ,  H , Radius ,    Color*/

    /* CPU Outline, */
    tft.drawRoundRect  (13,  22, 88,  89, 6,    ILI9341_WHITE);

    /* GPU Outline, */
    tft.drawRoundRect  (13, 144, 88,  89, 6,    ILI9341_WHITE);

    tft.drawRoundRect  (2,   0  , 200, 120, 8,    ILI9341_WHITE);
    tft.drawRoundRect  (202,   0  , 117, 120, 8,    ILI9341_RED);

    tft.drawRoundRect  (2, 123, 200, 114, 8,    ILI9341_GREEN);
    tft.drawRoundRect  (202, 123, 117, 114, 8,    ILI9341_YELLOW);


    //------------------------------------CPU/GPU/RAM BMP IMAGES--------------------------------------------

    /* Blank CPU PCB BMP, */
    tft.drawBitmap(16, 25, CPU3_BMP, 82, 82, ILI9341_GREEN);

#ifdef INTEL_CPU
    //tft.drawBitmap(16, 25, IntelCoreOnly_BMP, 88, 82, ILI9341_BLUE);
    tft.drawBitmap(16, 25, IntelCoreOnly_BMP, 88, 82, ILI9341_SILVER);
#endif

#ifdef AMD_CPU
    tft.drawBitmap(16, 25, AMDCoreOnly_BMP, 88, 82, ILI9341_RED);
#endif


    tft.setTextSize(1);
    tft.setCursor(116, 8); // (Left/Right, UP/Down)
    tft.println("Temp    Load"); // CPU

    tft.setTextSize(3);
    tft.setCursor(1, 132); // (Left/Right, UP/Down)

#ifdef NVIDIA_GRAPHICS
    tft.drawBitmap(16, 148, Nvidia_Logo_BMP, 82, 82, ILI9341_GREEN); // Nvidia Logo
#endif

#ifdef AMD_GRAPHICS
    tft.drawBitmap(16, 148, RADEON_Logo_BMP, 82, 82, ILI9341_RED); // Nvidia Logo
#endif

#ifdef INTEL_GRAPHICS
    //tft.fillRoundRect  (14, 141, 86,  87, 5,    ILI9341_BLUE);  // INTEL GPU Logo
    tft.drawBitmap(13, 147, IntelCoreOnly_BMP, 88, 82, ILI9341_BLUE);
#endif


    //---------------------------------------CPU & GPU Hardware ID---------------------------------------------------------

    /* CPU & GPU Hardware ID */

   
      tft.setTextSize(1);
      tft.setCursor(16, 8);// (Left/Right, UP/Down)
      tft.println(getCpuName());


      tft.setTextSize(1);
      tft.setCursor(16, 130);  // Position GPU Name
      tft.println(getGpuName());
    

    //------------------------------------------------------RX indicator---------------------------------------------------

    tft.setCursor(302, 200);
    tft.print("RX");
    tft.fillCircle(306, 222, 7, ILI9341_RED);// Landscape Flash RX top right corner when updating
    tft.drawCircle(306, 222, 8, ILI9341_WHITE);

    //--------------------------------------------DATA CLEARING BOXES------------------------------------------------------

    /* New Update Clearing Boxes see: tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);*/
#ifdef Debug

    tft.setTextColor(ILI9341_WHITE, ILI9341_RED);
#define ILI9341_updateBox ILI9341_GREY  // Fill boxes grey for text alignment 

#else

#define ILI9341_updateBox  ILI9341_BLACK // Default for normal running

#endif

    //------------------------------------------ CPU Load/Temp -------------------------------------------------

    /* CPU Display String */
    String cpuTemp = getValue("CT");
    String cpuLoad = getValue("CL");

    //Char erase and spacing adjust, MaDerer
    /* CPU TEMP,*/
    if (cpuTemp.length() == 1) cpuTemp = " "  + cpuTemp;

    /* CPU LOAD,*/
    if (cpuLoad.length() == 2) cpuLoad = " "  + cpuLoad;
    if (cpuLoad.length() == 1) cpuLoad = "  " + cpuLoad;

    /* CPU TEMPERATURE */
    tft.setTextSize(2);
    tft.setCursor(109 , 25);// (Left/Right, UP/Down)
    tft.print(cpuTemp);  // CPU TEMP
    tft.setTextSize(1);
    tft.print((char)247); //Degrees Symbol
    tft.print("C");       // Centigrade Symbol

    /* CPU LOAD, ALL CORES */
    tft.setTextSize(2);
    tft.setCursor(152, 25);// (Left/Right, UP/Down)
    tft.print(cpuLoad);  // CPU Load
    tft.setTextSize(1);
    tft.print("%");

    //------------------------------------------ CPU Freq -------------------------------------------------

    /* CPU Freq Display String*/
    String cpuClockString = getValue("CC");

    //Char erase and spacing adjust, MaDerer
    while (cpuClockString.length() < 4) cpuClockString = " " + cpuClockString;

    /* CPU OVERCLOCK Freq Gain */
    double cpuOverclockGain = atof(cpuClockString.c_str());
    double  cpuOverclockSum = cpuOverclockGain - CPU_BOOST; //values in Mhz

    double cpuOverclockGainPercentSum = cpuOverclockSum / (CPU_BOOST / 100); // % of gain over Stock CPU
    /* CPU  Freq Display */
    tft.setTextSize(3);
    tft.setCursor(105, 55);// (Left/Right, UP/Down)
    tft.print(cpuClockString);
    tft.setTextSize(1);
    tft.print("MHz");

    /* CPU Power */
    String cpuPowerString =  getValue("CP");
    //Char erase and spacing adjust, MaDerer
    while (cpuPowerString.length() < 6) cpuPowerString = " " + cpuPowerString;
    tft.setTextSize(1);
    tft.setCursor(109, 80);   // (Left/Right, UP/Down)
    tft.print("Power :");
    tft.print(cpuPowerString); //GPU Power Watts

    tft.setTextSize(1);
    tft.print("w");

    /* CPU OVERCLOCK Display Freq Gain */
    //tft.setCursor(225, 48);// (Left/Right, UP/Down)
    //tft.setTextSize(1);
    //tft.print ("CPU O/C: ");
    //tft.print(cpuOverclockSum, 0); // Show Value in MHZ
    //tft.println ("MHz");

#ifdef  enable_ShowFrequencyGain

    /* CPU OVERCLOCK Display Freq Gain in MHz */
    tft.setCursor(109, 94);// (Left/Right, UP/Down)

    //tft.print ("O/C:+ ");

#ifdef ShowFrequencyGainMHz
    tft.setTextSize(2);
    tft.print ("+");
    tft.print(cpuOverclockSum, 0);            // Show Value in MHz
    tft.setTextSize(1);
    tft.println ("MHz");
#endif
#ifdef ShowFrequencyGain%
    tft.setTextSize(2);
    if (cpuOverclockGainPercentSum > 0)
      tft.print (" +");
    else
      tft.print (" ");
      
    tft.print(cpuOverclockGainPercentSum, 0); // Show Value in %
    tft.println ("%");
#endif
#endif

    //------------------------------------------ GPU Load/Temp -------------------------------------------------

    /* GPU Display String */
    String gpuTemp = getValue("GT");
    String gpuLoad = getValue("GL");

    //Char erase and spacing adjust, MaDerer
    /* GPU TEMP,*/
    if (gpuTemp.length() == 1) gpuTemp = " "  + gpuTemp;
    /* GPU LOAD,*/
    if (gpuLoad.length() == 2) gpuLoad = " "  + gpuLoad;
    if (gpuLoad.length() == 1) gpuLoad = "  " + gpuLoad;

    /* GPU TEMPERATURE */
    tft.setTextSize(2);
    tft.setCursor(109, 144);// (Left/Right, UP/Down)
    tft.print(gpuTemp);
    tft.setTextSize(1);

    tft.print((char)247); //Degrees Symbol
    tft.print("C");       // Centigrade Symbol

    /* GPU LOAD */
    tft.setTextSize(2);
    tft.setCursor(152 , 144); // (Left/Right, UP/Down)
    tft.print(gpuLoad);

    tft.setTextSize(1);
    tft.print("%");



    //------------------------------------------ GPU Freq/Temp -------------------------------------------------

    /* GPU temp V's GPU freq to check for throttling and max 'GPU Boost' */

    /* GPU Core Freq, */
    String gpuCoreClockString =  getValue("GCC");

    //Char erase and spacing adjust, MaDerer
    while (gpuCoreClockString.length() < 4) gpuCoreClockString = " " + gpuCoreClockString;

    /* GPU VRAM Freq, */
    String gpuMemClockString = getValue("GMC");


    //Char erase and spacing adjust, MaDerer
    while (gpuMemClockString.length() < 4) gpuMemClockString = " " + gpuMemClockString;
    
    /* GPU OVERCLOCK Freq Gain in MHz, */
    double gpuOverclockGain = atof(gpuCoreClockString.c_str());
    double  gpuOverclockSum = gpuOverclockGain - GPU_BOOST; //values in Mhz

    /* GPU OVERCLOCK Freq Gain in Percent, eg: 1683MHz/100 = 16.83MHz(1%) , (OC Gain)254MHz / 16.83MHz(1%) = 15.09%,*/
    double gpuOverclockGainPercentSum = gpuOverclockSum / (GPU_BOOST / 100); // % of gain over Stock GPU


#ifdef  enable_ShowFrequencyGain
    /* GPU OVERCLOCK Display Freq Gain, */
    //tft.setCursor(225, 56);// (Left/Right, UP/Down)
    tft.setCursor(109, 211);
    //tft.print ("GPU O/C: ");

#ifdef ShowFrequencyGainMHz
    tft.setTextSize(2);
    tft.print ("+");
    tft.print(gpuOverclockSum, 0);            // Show Value in MHz
    tft.setTextSize(1);
    tft.println ("MHz");
#endif
#ifdef ShowFrequencyGain%
    tft.setTextSize(2);
    if (gpuOverclockGainPercentSum > 0)
        tft.print (" +");
    else
        tft.print (" ");  
    tft.print(gpuOverclockGainPercentSum, 0); // Show Value in %
    tft.println ("%");
#endif

#endif


    /* GPU Power */  // Nvidia Driver 457.51 works. Broken in Driver Version: 460.79 460.89
    String gpuPowerString =  getValue("GP");
    //Char erase and spacing adjust, MaDerer
    while (gpuPowerString.length() < 6) gpuPowerString = " " + gpuPowerString;


    tft.setCursor(109, 175);// (Left/Right, UP/Down)
    tft.setTextSize(1);
    tft.print("VRAM  :");
    tft.print(gpuMemClockString);

    tft.setTextSize(1);
    tft.print("MHz");

    tft.setTextSize(1);
    tft.setCursor(109, 185);   // (Left/Right, UP/Down)
    tft.print("Power :");
    tft.print(gpuPowerString); //GPU Power Watts

    tft.setTextSize(1);
    tft.print("w");

    tft.setTextSize(1);
    tft.setCursor(109, 195);  // (Left/Right, UP/Down)
    tft.print("Core  :");
    tft.print(gpuCoreClockString);

    tft.setTextSize(1);
    tft.print("MHz");       // Centigrade Symbol

    //----------------------------------------------GPU Memory----------------------------------------------------------

    tft.setCursor(110, 130);  // Position GPU Total Memory

    /*GPU Memory Used */
    String gpuMemoryUsedString =  getValue("GMU");
    //Char erase and spacing adjust, MaDerer
    while (gpuMemoryUsedString.length() < 4) gpuMemoryUsedString = " " + gpuMemoryUsedString;

    double usedGPUmem = atof(gpuMemoryUsedString.c_str());
    double usedGPUmemSum = usedGPUmem / 1024;    // divide by 1024 to get the correct value
    float  usedGPUmemSumDP = usedGPUmemSum ;     // float to handle the decimal point when printed (totalGPUmemSumDP, 0)
    tft.print(usedGPUmemSumDP, 2); // Show Value in GB
    tft.print("GB");

    tft.print(" / ");

    /*GPU Memory Total */
    String gpuMemoryString = getValue("GMT");
    //Char erase and spacing adjust, MaDerer
    while (gpuMemoryString.length() < 4) gpuMemoryString = " " + gpuMemoryString;

    double totalGPUmem = atof(gpuMemoryString.c_str());
    double totalGPUmemSum = totalGPUmem / 1024;    // divide by 1024 to get the correct value
    float  totalGPUmemSumDP = totalGPUmemSum ;     // float to handle the decimal point when printed (totalGPUmemSumDP, 0)

    tft.setTextSize(1);
    tft.print(totalGPUmemSumDP, 0); // Show Value in GB
    tft.print("GB");


    //----------------------------------------SYSTEM RAM USAGE---------------------------------------------------
    tft.drawBitmap(210, 8, Trident2_RAM_BMP, 100, 40, ILI9341_YELLOW);

    /* SYSTEM RAM String */
    String ramString   =  getValue("RU");
    //Char erase and spacing adjust, MaDerer
    while (ramString.length() < 4) ramString = " " + ramString;

    /* SYSTEM RAM AVALABLE String */
    String AramString =  getValue("RA");
    //Char erase and spacing adjust, MaDerer
    while (AramString.length() < 5) AramString = " " + AramString;

    /* SYSTEM RAM TOTAL String */
    double intRam = atof(ramString.c_str());
    double intAram = atof(AramString.c_str());
    //double  intRamSum = intRam + intAram;
    float  intRamSum = intRam + intAram; //float to handle the decimal point when printed (intRamSum,0)

    /* RAM & TOTAL */
    tft.setCursor(210 , 55); // (Left/Right, UP/Down)
    tft.println("USED      TOTAL");

    tft.setCursor(206, 70); // (Left/Right, UP/Down)
    tft.setTextSize(2);
    tft.print(ramString)    ; tft.setTextSize(0); tft.print("GB");
    tft.print(" ");
    tft.setTextSize(2);
    tft.print(intRamSum, 0) ; tft.setTextSize(0); tft.print("GB");
    
    tft.setCursor(210, 96); // (Left/Right, UP/Down)
    tft.setTextSize(1);
    tft.print("Slot#2 Temp: ");
    tft.print(getValue("RT1")); //GPU Power Watts
    tft.print((char)247);
    tft.print("c");

    tft.setCursor(210, 106); // (Left/Right, UP/Down)
    tft.setTextSize(1);
    tft.print("Slot#4 Temp: ");
    tft.print(getValue("RT2")); //GPU Power Watts
    tft.print((char)247);
    tft.print("c");
    //------------------------------------------ Temperatures ------------------------------------------------
    tft.setCursor(210 , 130); // (Left/Right, UP/Down)
    tft.println("C:\\   VRM   MB");
     tft.drawFastHLine(210, 150, 101,  ILI9341_SILVER);

    tft.setCursor(210 , 140);
    tft.print(getValue("OSDT"));
    tft.print((char)247);  //Degrees Symbol
    tft.print("C  ");  
    tft.print(getValue("VRMT"));
    tft.print((char)247);  //Degrees Symbol
    tft.print("C  ");  
    tft.print(getValue("MBT"));
    tft.print((char)247);  //Degrees Symbol
    tft.print("C");  

    tft.setCursor(210 , 155);
    tft.print("Intake :");
    String intakeFan = getValue("FI");
    while (intakeFan.length() < 4) intakeFan = " " + intakeFan;
    tft.print(intakeFan);
    tft.print(" RPM");  
    tft.setCursor(210 , 165);
    tft.print("Exhaust:");
    String exhaustFan = getValue("FO");
    while (exhaustFan.length() < 4) exhaustFan = " " + exhaustFan;
    tft.print(exhaustFan);
    tft.print(" RPM");  
    tft.setCursor(210 , 175);
    tft.print("Rear   :");
    String rearFan = getValue("FR");
    while (rearFan.length() < 4) rearFan = " " + rearFan;
    tft.print(rearFan);
    tft.print(" RPM");  
    tft.setCursor(210 , 185);
    tft.print("Pump   :");
    String pump = getValue("WP");
    while (pump.length() < 4) pump = " " + pump;
    tft.print(pump);
    tft.print(" RPM");  

    tft.drawBitmap(210, 195, WaterCoolingBMP, 40 , 40, ILI9341_AQUA); 
    tft.setTextSize(1);
    tft.setCursor(255 , 198);
    tft.print("Water");  
    tft.setTextSize(2);
    tft.setCursor(255 , 215);
    tft.print(getValue("WOT"));
    tft.setTextSize(1);
    tft.print((char)247);  //Degrees Symbol
    tft.print("C");  
    
    
    //------------------------------------------ RX indicator Clear------------------------------------------------

    delay(TX_LED_Delay); // TX blink delay
    tft.fillCircle(306, 222, 7, ILI9341_BLACK);// Flash top right corner when updating

    //-------------------------------------------------------------------------------------------------------------

    displayDraw = 1;

    //--------------------------Trigger an event when CPU or GPU threshold is met ---------------------------------

#ifdef enable_BoostIndicator
    CustomTriggerCPU_BOOST_LSNB( cpuClockString.toInt     ()); // CPU Frequency
    CustomTriggerGPU_BOOST_LSNB( gpuCoreClockString.toInt ()); // GPU Frequency
#endif

#ifdef enable_ThrottleIndicator
    CustomTriggerCPU_ThrottleIndicator_LSNB( cpuTemp.toInt() ); //  CPU TJMax/Throttle Incicator BMP
    CustomTriggerGPU_ThrottleIndicator_LSNB( gpuTemp.toInt() ); //  GPU TJMax/Throttle Incicator BMP
#endif

    inputString = "";
    stringComplete = false;
    //tft.fillScreen(ILI9341_BLACK);

  }
}


/*
    _____          _                    _______   _
   / ____|        | |                  |__   __| (_)
  | |    _   _ ___| |_ ___  _ __ ___      | |_ __ _  __ _  __ _  ___ _ __ ___
  | |   | | | / __| __/ _ \| '_ ` _ \     | | '__| |/ _` |/ _` |/ _ \ '__/ __|
  | |___| |_| \__ \ || (_) | | | | | |    | | |  | | (_| | (_| |  __/ |  \__ \
   \_____\__,_|___/\__\___/|_| |_| |_|    |_|_|  |_|\__, |\__, |\___|_|  |___/
                                                    __/ | __/ |
                                                   |___/ |___/

  Custom Trigger functions, when CPU or GPU threshold are met
*/


// -------------------  CPU Throttle Indicator Event Landscape --------------------

void CustomTriggerCPU_ThrottleIndicator_LSNB(int cpuDegree ) {  // i5-9600k TJMax is 100c
  float CPUtempfactor = cpuDegree ;

  if (CPUtempfactor >= CPU_TJMAX ) {  // TJ Max for the Intel 9900K 100c

    /* CPU Junction Max Throttle Temp, */
    tft.fillRoundRect  (107, 91, 86, 20, 4, ILI9341_RED);   //
    tft.setTextSize(2);
    tft.setCursor(121, 94);
    tft.setTextColor(ILI9341_BLACK);

    tft.println("TJMax"); // CPU Turbo Clock
  }
}

// -------------------  GPU Throttle Indicator Event Landscape --------------------

void CustomTriggerGPU_ThrottleIndicator_LSNB(int gpuDegree ) {
  float GPUtempfactor = gpuDegree ;

  if (GPUtempfactor >= GPU_TJMAX ) {  //GTX 1080 TJMax = 83c

    /* GPU Junction Max Throttle Temp, */
    tft.fillRoundRect  (107, 208, 86, 20, 4, ILI9341_RED);   //

    tft.setTextSize(2);
    tft.setCursor(121, 213);
    tft.setTextColor(ILI9341_BLACK);

    tft.println("TJMax"); // GPU Boost Clock

  }
}

// -------------------  CPU Turbo Boost Indicator Event Landscape --------------------

void CustomTriggerCPU_BOOST_LSNB(int cpuClockString ) {
  float CPUboostfactor = cpuClockString;

  delay(350); // Small delay so Turbo frequency gains stay on screen longer
  tft.drawRoundRect  (106, 90, 88, 22, 4, ILI9341_WHITE); //

  if (CPUboostfactor >  CPU_BOOST) {  // i5-9600k boost is 3700Mhz to 4700Mhz
    //Do Something!!!

#ifdef CPU_OverClocked //Do Nothing!!

    tft.fillRoundRect  (107, 91, 86, 20, 4, ILI9341_BLACK);   //
    tft.setTextSize(1);
    tft.setCursor(118, 97);
    tft.setTextColor(ILI9341_WHITE);
    tft.println("OVERCLOCKED"); // CPU Turbo Clock

#else

    /* CPU Turbo Clock, */
    tft.fillRoundRect  (107, 91, 86, 20, 4, ILI9341_GREEN);   //
    tft.setTextSize(2);
    tft.setCursor(121, 94);
    tft.setTextColor(ILI9341_BLACK);
    tft.println("BOOST");
#endif

  }
}

// -------------------  GPU Boost Clock Indicator Event Landscape --------------------

void CustomTriggerGPU_BOOST_LSNB(int gpuCoreClockString ) {

  float GPUboostfactor = gpuCoreClockString ;

  tft.drawRoundRect  (106, 207, 88, 22, 4, ILI9341_WHITE); //

  if (GPUboostfactor >  GPU_BOOST) {  //GTX 1080 boost = 1607Mhz to 1733mhz

    /* GPU Boost Clock, */
    tft.fillRoundRect  (107, 208, 86, 20, 4, ILI9341_GREEN);   //

    tft.setTextSize(2);
    tft.setCursor(121, 211);
    tft.setTextColor(ILI9341_BLACK);

    tft.println("BOOST");
  }
}
